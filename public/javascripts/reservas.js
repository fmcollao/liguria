$(function(){


	//funcion ajax eliminar producto
	$('#tbl-reservas #btn-eliminar').click(function(){				

		var elemento = $(this);	
		var id = elemento.parent().parent().find('#id_reserva').text();

		var confirmar = confirm('Desea eliminar la reserva');

		if(confirmar){
			$.ajax({
				url : 'http://localhost:3000/eliminarreserva',
				method : 'post',
				data : {id : id},
				success : function(res){
					if(res.res){
						elemento.parent().parent().remove();
					}
				}
			});
		}		
		
	});


});