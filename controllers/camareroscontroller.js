var mysql = require('mysql');
var dateFormat = require('dateformat');

//productos controller

module.exports = {

	//funciones del controlador 

	getCamareros : function(req, res, next){
		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		var camareros = null;

		db.query('SELECT * FROM camareros', function(err, rows, fields){
			if(err) throw err;

			camareros = rows;
			db.end();

			res.render('camareros/camareros', {camareros : camareros});
		});		
	},

	getNuevoCamarero : function(req, res, next){
		res.render('camareros/nuevo');
	},

	postNuevoCamarero : function(req, res, next){

		var fechaactual = new Date();
		var fecha = dateFormat(fechaactual, 'yyyy-mm-dd h:MM:ss');

		var camarero = {
			run : req.body.run,
			nombre : req.body.nombre,
			apellidoPaterno : req.body.apellidoPaterno,
			apellidoMaterno : req.body.apellidoMaterno,
			telefono : req.body.telefono,
			email : req.body.email,
			fecha_creacion : fecha
		}

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		db.query('INSERT INTO camareros SET ?', camarero, function(err, rows, fields){
			if(err) throw err;

			db.end();
		});

		res.render('camareros/nuevo', {info : 'Camarero creado correctamente'});
		//console.log(req.body);
	},

	eliminarCamarero : function(req, res, next){
		var id = req.body.id;

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		var respuesta = {res: false};

		db.query('DELETE FROM camareros WHERE id = ?', id, function(err, rows, fields){
			if(err)throw err;

			db.end();
			respuesta.res = true;

			res.json(respuesta);
		});
	},

	getModificarCamarero : function(req, res, next){
		var id = req.params.id;

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		var camarero = null;

		db.query('SELECT * FROM camareros WHERE id = ?', id, function(err, rows, fields){
			if(err) throw err;

			camarero = rows;
			db.end();

			res.render('camareros/modificar', {camarero: camarero});
		});
	},

	postModificarCamarero : function(req, res, next){

		var camarero = {
			run : req.body.run,
			nombre : req.body.nombre,
			apellidoPaterno : req.body.apellidoPaterno,
			apellidoMaterno : req.body.apellidoMaterno,
			telefono : req.body.telefono,
			email : req.body.email,			
		};

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		db.query('UPDATE camareros SET ? WHERE ?', [camarero, {id : req.body.id_camarero}], function(err, rows, fields){
			if(err) throw err;
			db.end();
		});

		res.redirect('/camareros');

	}
}