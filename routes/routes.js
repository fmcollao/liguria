var express = require('express');
var router = express.Router();

var controllers = require('.././controllers');

/* GET home page. */
router.get('/', controllers.homecontroller.index);


//rutas para productos
router.get('/productos', controllers.productoscontroller.getProductos);
router.get('/nuevoproducto', controllers.productoscontroller.getNuevoProducto);
router.post('/crearproducto', controllers.productoscontroller.postNuevoProducto);
router.post('/eliminarproducto', controllers.productoscontroller.eliminarProducto);
router.get('/modificarproducto/:id', controllers.productoscontroller.getModificarProducto);
router.post('/editarproducto', controllers.productoscontroller.postModificarProducto);

//rutas para clientes
router.get('/clientes', controllers.clientescontroller.getClientes);
router.get('/nuevocliente', controllers.clientescontroller.getNuevoCliente);
router.post('/crearcliente', controllers.clientescontroller.postNuevoCliente);
router.post('/eliminarcliente', controllers.clientescontroller.eliminarCliente);
router.get('/modificarcliente/:id', controllers.clientescontroller.getModificarCliente);
router.post('/editarcliente', controllers.clientescontroller.postModificarCliente);

//rutas para camareros
router.get('/camareros', controllers.camareroscontroller.getCamareros);
router.get('/nuevocamarero', controllers.camareroscontroller.getNuevoCamarero);
router.post('/crearcamarero', controllers.camareroscontroller.postNuevoCamarero);
router.post('/eliminarcamarero', controllers.camareroscontroller.eliminarCamarero);
router.get('/modificarcamarero/:id', controllers.camareroscontroller.getModificarCamarero);
router.post('/editarcamarero', controllers.camareroscontroller.postModificarCamarero);

//rutas para reservas
router.get('/reservas', controllers.reservascontroller.getReservas);
router.get('/nuevoreserva', controllers.reservascontroller.getNuevoReserva);
router.post('/crearreserva', controllers.reservascontroller.postNuevoReserva);
router.post('/eliminarreserva', controllers.reservascontroller.eliminarReserva);
router.get('/modificarreserva/:id', controllers.reservascontroller.getModificarReserva);
router.post('/editarreserva', controllers.reservascontroller.postModificarReserva);

module.exports = router;
