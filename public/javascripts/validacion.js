$(function(){
	$('.form-nuevoproducto form').form({
		nombre : {
			identifier : 'nombre',
			rules : [
				{
					type : 'empty',
					prompt : 'Por favor ingrese un nombre'
				}
			]
		},

		precio : {
			identifier : 'precio',
			rules : [
				{
					type: 'empty',
					prompt : 'Por favor ingrese un precio'
				},
				{
					type: 'number',
					prompt: 'El precio debe ser numerico'
				}
			]
		},

		stock : {
			identifier : 'stock',
			rules : [
				{
					type: 'empty',
					prompt : 'Por favor ingrese el stock'
				},
				{
					type: 'integer',
					prompt: 'El stock debe ser un numero entero'
				}
			]
		}
	});

	$('.form-nuevocliente form').form({
		nombre : {
			identifier : 'nombre',
			rules : [
				{
					type : 'empty',
					prompt : 'Por favor ingrese un nombre'
				}
			]
		},

		run : {
			identifier : 'run',
			rules : [
				{
					type: 'empty',
					prompt : 'Por favor ingrese un RUN'
				}
			]
		},

		apellidoPaterno : {
			identifier : 'apellidoPaterno',
			rules : [
				{
					type: 'empty',
					prompt : 'Por favor ingrese un Apellido Paterno'
				}
			]
		},

		apellidoMaterno : {
			identifier : 'apellidoMaterno',
			rules : [
				{
					type: 'empty',
					prompt : 'Por favor ingrese un Apellido Materno'
				}
			]
		},

		telefono : {
			identifier : 'telefono',
			rules : [
				{
					type: 'empty',
					prompt : 'Por favor ingrese un Telefono'
				}
			]
		},

		email : {
			identifier : 'email',
			rules : [
				{
					type: 'empty',
					prompt : 'Por favor ingrese un email'
				}
			]
		},
	});
});