var mysql = require('mysql');
var dateFormat = require('dateformat');

//productos controller

module.exports = {

	//funciones del controlador 

	getClientes : function(req, res, next){
		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		var clientes = null;

		db.query('SELECT * FROM clientes', function(err, rows, fields){
			if(err) throw err;

			clientes = rows;
			db.end();

			res.render('clientes/clientes', {clientes : clientes});
		});		
	},

	getNuevoCliente : function(req, res, next){
		res.render('clientes/nuevo');
	},

	postNuevoCliente : function(req, res, next){

		var fechaactual = new Date();
		var fecha = dateFormat(fechaactual, 'yyyy-mm-dd h:MM:ss');

		var cliente = {
			run : req.body.run,
			nombre : req.body.nombre,
			apellidoPaterno : req.body.apellidoPaterno,
			apellidoMaterno : req.body.apellidoMaterno,
			telefono : req.body.telefono,
			email : req.body.email,
			fecha_creacion : fecha
		}

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		db.query('INSERT INTO clientes SET ?', cliente, function(err, rows, fields){
			if(err) throw err;

			db.end();
		});

		res.render('clientes/nuevo', {info : 'Cliente creado correctamente'});
		//console.log(req.body);
	},

	eliminarCliente : function(req, res, next){
		var id = req.body.id;

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		var respuesta = {res: false};

		db.query('DELETE FROM clientes WHERE id = ?', id, function(err, rows, fields){
			if(err)throw err;

			db.end();
			respuesta.res = true;

			res.json(respuesta);
		});
	},

	getModificarCliente : function(req, res, next){
		var id = req.params.id;

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		var cliente = null;

		db.query('SELECT * FROM clientes WHERE id = ?', id, function(err, rows, fields){
			if(err) throw err;

			cliente = rows;
			db.end();

			res.render('clientes/modificar', {cliente: cliente});
		});
	},

	postModificarCliente : function(req, res, next){

		var cliente = {
			run : req.body.run,
			nombre : req.body.nombre,
			apellidoPaterno : req.body.apellidoPaterno,
			apellidoMaterno : req.body.apellidoMaterno,
			telefono : req.body.telefono,
			email : req.body.email,			
		};

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		db.query('UPDATE clientes SET ? WHERE ?', [cliente, {id : req.body.id_cliente}], function(err, rows, fields){
			if(err) throw err;
			db.end();
		});

		res.redirect('/clientes');

	}
}