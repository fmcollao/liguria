var mysql = require('mysql');
var dateFormat = require('dateformat');

//reservas controller

module.exports = {

	//funciones del controlador 

	getReservas : function(req, res, next){
		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		var reservas = null;

		db.query('SELECT * FROM reservas', function(err, rows, fields){
			if(err) throw err;

			reservas = rows;
			db.end();

			res.render('reservas/reservas', {reservas : reservas});
		});		
	},

	getNuevoReserva : function(req, res, next){
		res.render('reservas/nuevo');
	},

	postNuevoReserva : function(req, res, next){

		var fechaactual = new Date();
		var fecha = dateFormat(fechaactual, 'yyyy-mm-dd h:MM:ss');

		var reserva = {
			runCliente : req.body.runCliente,
			runCamarero : req.body.runCamarero,
			fecha_reserva : req.body.fechaReserva,
			cantidadComensales : req.body.cantidadPersonas,
			fecha_creacion : fecha
		}

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		db.query('INSERT INTO reservas SET ?', reserva, function(err, rows, fields){
			if(err) throw err;

			db.end();
		});

		res.render('reservas/nuevo', {info : 'Reserva creada correctamente'});
		//console.log(req.body);
	},

	eliminarReserva : function(req, res, next){
		var id = req.body.id;

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		var respuesta = {res: false};

		db.query('DELETE FROM reservas WHERE id = ?', id, function(err, rows, fields){
			if(err)throw err;

			db.end();
			respuesta.res = true;

			res.json(respuesta);
		});
	},

	getModificarReserva : function(req, res, next){
		var id = req.params.id;

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		var reserva = null;

		db.query('SELECT * FROM reservas WHERE id = ?', id, function(err, rows, fields){
			if(err) throw err;

			reserva = rows;
			db.end();

			res.render('reservas/modificar', {reserva: reserva});
		});
	},

	postModificarReserva : function(req, res, next){

		var reserva = {
			runCliente : req.body.runCliente,
			runCamarero : req.body.runCamarero,
			fecha_reserva : req.body.fechaReserva,
			cantidadComensales : req.body.cantidadPersonas,
			//fecha_creacion : fecha			
		};

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		db.query('UPDATE reservas SET ? WHERE ?', [reserva, {id : req.body.id_reserva}], function(err, rows, fields){
			if(err) throw err;
			db.end();
		});

		res.redirect('/reservas');

	}
}