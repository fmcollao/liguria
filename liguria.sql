-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.19-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para nodejs
DROP DATABASE IF EXISTS `nodejs`;
CREATE DATABASE IF NOT EXISTS `nodejs` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `nodejs`;


-- Volcando estructura para tabla nodejs.camareros
DROP TABLE IF EXISTS `camareros`;
CREATE TABLE IF NOT EXISTS `camareros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `run` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `apellidoPaterno` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `apellidoMaterno` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `fecha_creacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla nodejs.camareros: ~1 rows (aproximadamente)
DELETE FROM `camareros`;
/*!40000 ALTER TABLE `camareros` DISABLE KEYS */;
INSERT INTO `camareros` (`id`, `run`, `nombre`, `apellidoPaterno`, `apellidoMaterno`, `telefono`, `email`, `fecha_creacion`) VALUES
	(1, '21623469-3', 'Pedro', 'Pardo', 'Peres', '4654665', 'fran.mora.collao@gmail.com', '2016-11-23 01:48:27');
/*!40000 ALTER TABLE `camareros` ENABLE KEYS */;


-- Volcando estructura para tabla nodejs.clientes
DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `run` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `apellidoPaterno` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `apellidoMaterno` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `telefono` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `fecha_creacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='tabla de clientes';

-- Volcando datos para la tabla nodejs.clientes: ~1 rows (aproximadamente)
DELETE FROM `clientes`;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`id`, `run`, `nombre`, `apellidoPaterno`, `apellidoMaterno`, `telefono`, `email`, `fecha_creacion`) VALUES
	(1, '21623469-3', 'Francisco Esteban', 'Mora', 'Collao', '56999999999', 'admin@admin.com', '2016-11-23 01:14:40');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;


-- Volcando estructura para tabla nodejs.productos
DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `precio` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `stock` int(11) DEFAULT '0',
  `fecha_creacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla nodejs.productos: ~1 rows (aproximadamente)
DELETE FROM `productos`;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` (`id`, `nombre`, `precio`, `stock`, `fecha_creacion`) VALUES
	(2, 'Pera de agua', '1500', 45, '2016-11-23 12:12:20');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;


-- Volcando estructura para tabla nodejs.reservas
DROP TABLE IF EXISTS `reservas`;
CREATE TABLE IF NOT EXISTS `reservas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `runCliente` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `runCamarero` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `fecha_reserva` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cantidadComensales` int(11) DEFAULT '0',
  `fecha_creacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='tabla de reservas';

-- Volcando datos para la tabla nodejs.reservas: ~1 rows (aproximadamente)
DELETE FROM `reservas`;
/*!40000 ALTER TABLE `reservas` DISABLE KEYS */;
INSERT INTO `reservas` (`id`, `runCliente`, `runCamarero`, `fecha_reserva`, `cantidadComensales`, `fecha_creacion`) VALUES
	(2, '21623469-3', '5681842-1', '2016-11-15 10:00:45', 5, '2016-11-23 03:03:00');
/*!40000 ALTER TABLE `reservas` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
