$(function(){


	//funcion ajax eliminar camarero
	$('#tbl-clientes #btn-eliminar').click(function(){				

		var elemento = $(this);	
		var id = elemento.parent().parent().find('#id_cliente').text();

		var confirmar = confirm('Desea eliminar el cliente');

		if(confirmar){
			$.ajax({
				url : 'http://localhost:3000/eliminarcliente',
				method : 'post',
				data : {id : id},
				success : function(res){
					if(res.res){
						elemento.parent().parent().remove();
					}
				}
			});
		}		
		
	});


});