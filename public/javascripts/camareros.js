$(function(){


	//funcion ajax eliminar camarero
	$('#tbl-camareros #btn-eliminar').click(function(){				

		var elemento = $(this);	
		var id = elemento.parent().parent().find('#id_camarero').text();

		var confirmar = confirm('Desea eliminar el camarero');

		if(confirmar){
			$.ajax({
				url : 'http://localhost:3000/eliminarcamarero',
				method : 'post',
				data : {id : id},
				success : function(res){
					if(res.res){
						elemento.parent().parent().remove();
					}
				}
			});
		}		
		
	});


});